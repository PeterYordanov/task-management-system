USE [master]
GO

/****** Object:  Database [taskmanagementsystem]    Script Date: 4/20/2020 12:01:41 PM ******/
CREATE DATABASE [taskmanagementsystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'taskmanagementsystem_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\taskmanagementsystem.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'taskmanagementsystem_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\taskmanagementsystem.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [taskmanagementsystem] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [taskmanagementsystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [taskmanagementsystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [taskmanagementsystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [taskmanagementsystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [taskmanagementsystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [taskmanagementsystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET RECOVERY FULL 
GO
ALTER DATABASE [taskmanagementsystem] SET  MULTI_USER 
GO
ALTER DATABASE [taskmanagementsystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [taskmanagementsystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [taskmanagementsystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [taskmanagementsystem] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [taskmanagementsystem] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'taskmanagementsystem', N'ON'
GO
ALTER DATABASE [taskmanagementsystem] SET QUERY_STORE = OFF
GO
USE [taskmanagementsystem]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 4/20/2020 12:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CarSpecification] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 4/20/2020 12:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[CommentText] [varchar](250) NULL,
	[UserId] [int] NOT NULL,
	[TaskId] [int] NOT NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Task]    Script Date: 4/20/2020 12:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskTitle] [varchar](50) NOT NULL,
	[Description] [varchar](250) NULL,
	[CreatedDate] [date] NULL,
	[Deadline] [date] NULL,
	[AssignedDate] [date] NULL,
	[AssignedTo] [int] NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 4/20/2020 12:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Email] [varchar](50) NOT NULL,
	[Telephone] [varchar](50) NULL,
	[Role] [varchar](50) NULL,
	[Token] [varchar](max) NULL,
	[Password] [varchar](max) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_User]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Category_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Category_CategoryId]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_User_AssignedTo] FOREIGN KEY([AssignedTo])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_User_AssignedTo]
GO
USE [master]
GO
ALTER DATABASE [taskmanagementsystem] SET  READ_WRITE 
GO
