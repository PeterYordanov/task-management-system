﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.AuthHandler
{
    public class SessionManager 
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SessionManager(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string CurrentRole
        {
            get
            {
                string role = string.Empty;
                if (_httpContextAccessor.HttpContext.Session.Get("Role") != null)
                {
                    role = Encoding.UTF8.GetString(_httpContextAccessor.HttpContext.Session.Get("Role"));
                }
                return role;
            }
        }
        public string Email
        {
            get
            {
                string role = string.Empty;
                if (_httpContextAccessor.HttpContext.Session.Get("Email") != null)
                {
                    role = Encoding.UTF8.GetString(_httpContextAccessor.HttpContext.Session.Get("Email"));
                }
                return role;
            }
        }
        public string Token
        {
            get
            {
                string role = string.Empty;
                if (_httpContextAccessor.HttpContext.Session.Get("Token") != null)
                {
                    role = Encoding.UTF8.GetString(_httpContextAccessor.HttpContext.Session.Get("Token"));
                }
                return role;
            }
        }

        public int UserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsBackOffice { get; set; }
        public bool IsUser { get; set; }
        public bool IsStateLogged { get; set; }

        public void SetUserSession(UserViewModel user)
        {
            var httpContext = _httpContextAccessor.HttpContext;
            httpContext.Session.SetString("UserId", user.UserId.ToString());
            httpContext.Session.SetString("Email", user.Email);
            httpContext.Session.SetString("Role", user.Role);
            UserId = user.UserId;
            if (user.Role.Contains("Admin"))
             {
                IsAdmin = true;
            }
            if (user.Role.Contains("Back Office"))
            {
                IsBackOffice = true;
            }
            if (user.Role.Contains("User"))
            {
                IsUser = true;
            }
            httpContext.Session.SetString("Token", user.Token);
            httpContext.Session.SetString("IsStateLogged", "true");
            IsStateLogged = true;
        }

        public void ClearSession()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            httpContext.Session.SetString("Email", string.Empty);
            httpContext.Session.SetString("Role", string.Empty);
            httpContext.Session.SetString("Token", string.Empty);
            httpContext.Session.SetString("IsStateLogged", "false");
            IsStateLogged = false;
        }
    }
}
