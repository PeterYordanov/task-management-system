﻿using System.Collections.Generic;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public interface ICommentService<T> 
    {
        List<CommentViewModel> List();
        CommentViewModel Get(int id);
        CommentViewModel Update(CommentViewModel model);
        CommentViewModel Create(CommentViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}