﻿using System.Collections.Generic;
using Web.TaskManagementSystem.AuthHandler;
using Web.TaskManagementSystem.HttpHandler;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public class CategoryService<T> : GenericService, ICategoryService<T>
    {
        private readonly IHttpRequestService<CategoryViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public CategoryService(IHttpRequestService<CategoryViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<CategoryViewModel> List()
        {
            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                categories = _httpService.List(Constants.APIConstants.Categories, _sessionManager.Token);
            }
            return categories;
        }

        public CategoryViewModel Get(int id)
        {
            CategoryViewModel category = new CategoryViewModel();

            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                category = _httpService.Get(Constants.APIConstants.Categories, id, _sessionManager.Token);
            }
            return category;
        }

        public CategoryViewModel Update(CategoryViewModel model)
        {
            CategoryViewModel category = new CategoryViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                category = _httpService.Update(Constants.APIConstants.Categories, model, _sessionManager.Token);
            }
            return category;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public CategoryViewModel Create(CategoryViewModel model)
        {
            CategoryViewModel category = new CategoryViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                category = _httpService.Create(Constants.APIConstants.Categories, model, _sessionManager.Token);
            }
            return category;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.APIConstants.Categories, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
