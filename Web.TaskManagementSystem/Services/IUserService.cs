﻿using System.Collections.Generic;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public interface IUserService<T>
    {
        List<UserViewModel> List();
        UserViewModel Update(UserViewModel model);
        UserViewModel Create(UserViewModel model);
        UserViewModel SignIn(UserViewModel model);
        UserViewModel GetProfile();
        UserViewModel Get(int id);
        string SignUp(UserViewModel model);
        void UpdateProfile(UserViewModel user);
        void Settings(UserViewModel user);
        void SignOut();
        bool CanCreate();
        bool IsDeleted(int id);
    }
}
