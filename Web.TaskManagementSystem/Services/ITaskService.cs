﻿using DAL.TaskManagementSystem.CustomModel;
using System.Collections.Generic;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public interface ITaskService<T>
    {
        List<TaskViewModel> Search(FilterTaskViewModel model);
        List<TaskViewModel> List();
        TaskViewModel Get(int id);
        TaskViewModel Update(TaskViewModel model);
        TaskViewModel Create(TaskViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}
