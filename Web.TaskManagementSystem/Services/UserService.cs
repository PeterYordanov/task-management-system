﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.TaskManagementSystem.AuthHandler;
using Web.TaskManagementSystem.HttpHandler;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public class UserService<T> : GenericService, IUserService<T>
    {
        private readonly IHttpRequestService<UserViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public UserService(IHttpRequestService<UserViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<UserViewModel> List()
        {
            return _httpService.List(Constants.APIConstants.Users, _sessionManager.Token) ?? new List<UserViewModel>();
        }

        public UserViewModel Update(UserViewModel model)
        {
            UserViewModel user = model;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                user = _httpService.Update(Constants.APIConstants.Users, user, _sessionManager.Token);
            }
            return user;
        }

        public UserViewModel Create(UserViewModel model)
        {
            UserViewModel user = new UserViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                user = _httpService.Create(Constants.APIConstants.Users, model, _sessionManager.Token);
            }
            return user;
        }

        public UserViewModel SignIn(UserViewModel model)
        {
            UserViewModel user = _httpService.SignIn(model);
            if (user.Token != null && user.Role != null)
            {
                _sessionManager.SetUserSession(user);
            }
            return user;
        }

        public UserViewModel GetProfile()
        {
            return _httpService.GetProfile(Constants.APIConstants.Users, _sessionManager.UserId, _sessionManager.Token);
        }

        public UserViewModel Get(int id)
        {
            return _httpService.Get(Constants.APIConstants.Users, id, _sessionManager.Token) ?? new UserViewModel();
        }

        public string SignUp(UserViewModel model)
        {
            model.Role = "User";
            return _httpService.SignUp(model);
        }

        public void UpdateProfile(UserViewModel user)
        {
            _httpService.Update(Constants.APIConstants.Users, user, _sessionManager.Token);
            _sessionManager.ClearSession();
        }

        public void Settings(UserViewModel user)
        {
            _httpService.Update(Constants.APIConstants.Users, user, _sessionManager.Token);
            _sessionManager.ClearSession();
        }

        public void SignOut()
        {
            if (_sessionManager.IsStateLogged)
            {
                _sessionManager.ClearSession();
                _sessionManager.IsStateLogged = false;
            }
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.APIConstants.Users, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
