﻿using System.Collections.Generic;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public interface ICategoryService<T> 
    {
        List<CategoryViewModel> List();
        CategoryViewModel Get(int id);
        CategoryViewModel Update(CategoryViewModel model);
        CategoryViewModel Create(CategoryViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}