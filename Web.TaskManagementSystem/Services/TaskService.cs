﻿using System.Collections.Generic;
using Web.TaskManagementSystem.AuthHandler;
using Web.TaskManagementSystem.HttpHandler;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public class TaskService<T> : GenericService, ITaskService<T>
    {
        private readonly IHttpRequestService<TaskViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public TaskService(IHttpRequestService<TaskViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<TaskViewModel> Search(FilterTaskViewModel model)
        {
            _ = new List<TaskViewModel>();
            List<TaskViewModel> filtered = _httpService.Search(Constants.APIConstants.Search, model) ?? new List<TaskViewModel>();
            return filtered;
        }

        public List<TaskViewModel> List()
        {
            _ = new List<TaskViewModel>();
            List<TaskViewModel> tasks = _httpService.List(Constants.APIConstants.Tasks, _sessionManager.Token) ?? new List<TaskViewModel>();

            return tasks;
        }

        public TaskViewModel Get(int id)
        {
            _ = new TaskViewModel();
            TaskViewModel task = _httpService.Get(Constants.APIConstants.Tasks, id, _sessionManager.Token) ?? new TaskViewModel();
            return task;
        }

        public TaskViewModel Update(TaskViewModel model)
        {
            TaskViewModel task = new TaskViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                task = _httpService.Update(Constants.APIConstants.Tasks, model, _sessionManager.Token);
            }
            return task;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public TaskViewModel Create(TaskViewModel model)
        {
            TaskViewModel task = new TaskViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                task = _httpService.Create(Constants.APIConstants.Tasks, model, _sessionManager.Token);
            }
            return task;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.APIConstants.Tasks, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
