﻿using System.Collections.Generic;
using Web.TaskManagementSystem.AuthHandler;
using Web.TaskManagementSystem.HttpHandler;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Services
{
    public class CommentService<T> : GenericService, ICommentService<T>
    {
        private readonly IHttpRequestService<CommentViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public CommentService(IHttpRequestService<CommentViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<CommentViewModel> List()
        {
            List<CommentViewModel> comments = new List<CommentViewModel>();
            comments = _httpService.List(Constants.APIConstants.Comments, _sessionManager.Token);
            return comments;
        }

        public CommentViewModel Get(int id)
        {
            CommentViewModel comment = new CommentViewModel();
            comment = _httpService.Get(Constants.APIConstants.Comments, id, _sessionManager.Token);
            return comment;
        }

        public CommentViewModel Update(CommentViewModel model)
        {
            CommentViewModel comment = new CommentViewModel();
            if (_sessionManager.IsAdmin)
            {
                comment = _httpService.Update(Constants.APIConstants.Comments, model, _sessionManager.Token);
            }
            return comment;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsStateLogged == true)
            {
                result = true;
            }
            return result;
        }

        public CommentViewModel Create(CommentViewModel model)
        {
            CommentViewModel comment = new CommentViewModel();
            if (_sessionManager.IsStateLogged == true)
            {
                comment = _httpService.Create(Constants.APIConstants.Comments, model, _sessionManager.Token);
            }
            return comment;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin)
            {
                result = _httpService.Delete(Constants.APIConstants.Comments, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
