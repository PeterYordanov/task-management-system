﻿using System.ComponentModel.DataAnnotations;

namespace Web.TaskManagementSystem.ViewModels
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }

        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
    }
}
