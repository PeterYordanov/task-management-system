﻿using DAL.TaskManagementSystem.CustomModel;
using System.Collections.Generic;

namespace Web.TaskManagementSystem.ViewModels
{
    public class FilterTaskViewModel
    {
        public virtual ICollection<TaskViewModel> Tasks { get; set; }
        public virtual FilterTask Task { get; set; }
    }
}
