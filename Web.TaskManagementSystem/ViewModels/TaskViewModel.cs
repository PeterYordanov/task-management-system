﻿using DAL.TaskManagementSystem.CustomModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.TaskManagementSystem.ViewModels
{
    public class TaskViewModel
    {
        public int TaskId { get; set; }

        [Display(Name = "Created date")]
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Deadline")]
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        [DataType(DataType.Date)]

        [Display(Name = "Assigned Date")]
        public DateTime AssignedDate { get; set; }

        [Display(Name = "Title")]
        public string TaskTitle { get; set; }

        public string Description { get; set; }

        public int AssignedTo { get; set; }

        [ForeignKey("AssignedTo")]
        [Display(Name = "Assigned To")]
        public virtual UserViewModel User { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("Category Id")]
        [Display(Name = "Category")]
        public virtual CategoryViewModel Category { get; set; }
        public virtual ICollection<CategoryViewModel> Categories { get; set; }
        public virtual ICollection<UserViewModel> Users { get; set; }
        public virtual ICollection<CommentViewModel> Comments { get; set; }

    }
}