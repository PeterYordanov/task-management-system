﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.TaskManagementSystem.ViewModels
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }
        [Display(Name = "Comment")]
        public string CommentText { get; set; }
        public int TaskId { get; set; }
        [ForeignKey("TaskId")]
        public virtual TaskViewModel Task { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserViewModel User { get; set; }
        public virtual ICollection<UserViewModel> Users { get; set; }
        public virtual ICollection<TaskViewModel> Tasks { get; set; }
    }
}
