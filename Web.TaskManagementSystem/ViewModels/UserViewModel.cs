﻿using System.ComponentModel.DataAnnotations;

namespace Web.TaskManagementSystem.ViewModels
{
    public class UserViewModel 
    {
        public int UserId { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Telephone")]
        public string Telephone { get; set; }

        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }

        public string Token { get; set; }
    }
}
