﻿using AutoMapper;
using DAL.TaskManagementSystem.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Web.TaskManagementSystem.AuthHandler;
using Web.TaskManagementSystem.HttpHandler;
using Web.TaskManagementSystem.Services;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession();
            services.AddHttpContextAccessor();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<IHttpRequestService<UserViewModel>, HttpRequestService<UserViewModel>>();
            services.AddScoped<IHttpRequestService<TaskViewModel>, HttpRequestService<TaskViewModel>>();
            services.AddScoped<IHttpRequestService<CategoryViewModel>, HttpRequestService<CategoryViewModel>>();
            services.AddScoped<IHttpRequestService<CommentViewModel>, HttpRequestService<CommentViewModel>>();

            services.AddScoped<ICategoryService<CategoryViewModel>, CategoryService<CategoryViewModel>>();
            services.AddScoped<ITaskService<TaskViewModel>, TaskService<TaskViewModel>>();
            services.AddScoped<IUserService<UserViewModel>, UserService<UserViewModel>>();
            services.AddScoped<ICommentService<CommentViewModel>, CommentService<CommentViewModel>>();

            services.AddSingleton<SessionManager>();
            services.AddDistributedMemoryCache();
            var key = Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSession();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
