﻿using DAL.TaskManagementSystem.CustomModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.HttpHandler
{
    public class HttpRequestService<T> : IHttpRequestService<T> where T : class
    {
        public List<T> Search(string str, FilterTaskViewModel model)
        {
            var result = new List<T>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(Constants.APIConstants.HeaderName, Constants.APIConstants.HeaderValue);
                var serialized = JsonConvert.SerializeObject(model.Task);
                var deserialized = JsonConvert.DeserializeObject<Dictionary<string, string>>(serialized);
                var query = deserialized.Select((p) => p.Key.ToString() + "=" + Uri.EscapeDataString(p.Value)).Aggregate((p1, p2) => p1 + "&" + p2);
                string response = client.GetStringAsync(Constants.APIConstants.Url + str + "?" + query).Result;
                result = JsonConvert.DeserializeObject<List<T>>(response);
            }
            return result;
        }

        public List<T> List(string str, string bearerToken)
        {
            var result = new List<T>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(Constants.APIConstants.HeaderName, Constants.APIConstants.HeaderValue);
                if (str == Constants.APIConstants.Users || str == Constants.APIConstants.Tasks || str == Constants.APIConstants.Categories || str == Constants.APIConstants.Comments)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.APIConstants.Bearer, bearerToken);
                }
                var response = client.GetStringAsync(Constants.APIConstants.Url + str).Result;
                result = JsonConvert.DeserializeObject<List<T>>(response);
            }
            return result;
        }

        public T Get(string str, int id, string bearerToken)
        {
            T result = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(Constants.APIConstants.HeaderName, Constants.APIConstants.HeaderValue);
                if (str == Constants.APIConstants.Users || str == Constants.APIConstants.Tasks || str == Constants.APIConstants.Categories || str == Constants.APIConstants.Comments)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.APIConstants.Bearer, bearerToken);
                }
                var response = client.GetAsync(Constants.APIConstants.Url + str + "/" + id).Result;
                var body = client.GetStringAsync(Constants.APIConstants.Url + str + "/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<T>(body);
                }
            }
            return result;
        }

        public UserViewModel GetProfile(string str, int id, string bearerToken)
        {
            UserViewModel result;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(Constants.APIConstants.HeaderName, Constants.APIConstants.HeaderValue);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.APIConstants.Bearer, bearerToken);
                var response = client.GetStringAsync(Constants.APIConstants.Url + str + "/" + id).Result;
                result = JsonConvert.DeserializeObject<UserViewModel>(response);
            }
            return result;
        }

        public T Create(string str, T model, string bearerToken)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.APIConstants.Url + str);
                if (str == Constants.APIConstants.Users || str == Constants.APIConstants.Tasks || str == Constants.APIConstants.Categories || str == Constants.APIConstants.Comments)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.APIConstants.Bearer, bearerToken);
                }
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(Constants.APIConstants.HeaderValue);
                var response = client.PostAsync("", byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var body = response.Content.ReadAsStringAsync().Result;
                    model = JsonConvert.DeserializeObject<T>(body);
                }
            }
            return model;
        }

        public T Update(string str, T model, string bearerToken)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.APIConstants.Url + str);
                if (str == Constants.APIConstants.Users || str == Constants.APIConstants.Tasks || str == Constants.APIConstants.Categories || str == Constants.APIConstants.Comments)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.APIConstants.Bearer, bearerToken);
                }
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(Constants.APIConstants.HeaderValue);
                var response = client.PutAsync("", byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var body = response.Content.ReadAsStringAsync().Result;
                    model = JsonConvert.DeserializeObject<T>(body);
                }
            }
            return model;
        }

        public UserViewModel SignIn(UserViewModel model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.APIConstants.Url + Constants.APIConstants.SignIn);
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(Constants.APIConstants.HeaderValue);
                var response = client.PostAsync("", byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var body = response.Content.ReadAsStringAsync().Result;
                    model = JsonConvert.DeserializeObject<UserViewModel>(body);
                }
            }
            return model;
        }

        public string SignUp(T model)
        {
            var result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.APIConstants.Url + Constants.APIConstants.SignUp);
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(Constants.APIConstants.HeaderValue);
                var response = client.PostAsync("", byteContent).Result;
                if (!response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
            }
            return result;
        }

        public bool Delete(string str, int id, string bearerToken)
        {
            var result = true;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constants.APIConstants.Url + str);
                client.DefaultRequestHeaders.Add(Constants.APIConstants.HeaderName, Constants.APIConstants.HeaderValue);
                if (str == Constants.APIConstants.Users || str == Constants.APIConstants.Tasks || str == Constants.APIConstants.Categories || str == Constants.APIConstants.Comments)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.APIConstants.Bearer, bearerToken);
                }
                var response = client.DeleteAsync(Constants.APIConstants.Url + str + "/" + id).Result;
                if (!response.IsSuccessStatusCode)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
