﻿using System.Collections.Generic;

namespace Web.TaskManagementSystem.Constants
{
    public static class APIConstants
    {
        public const string Url = "https://localhost:44323/api/";
        public const string SignUp = "auth/signup";
        public const string SignIn = "auth/signin";
        public const string Users = "users";
        public const string Tasks = "tasks";
        public const string Search = "tasks/search";
        public const string Categories = "categories";
        public const string Comments = "comments";
        public const string HeaderName = "Accept";
        public const string HeaderValue = "application/json";
        public const string Bearer = "Bearer";
    }
}
