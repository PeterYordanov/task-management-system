﻿namespace Web.TaskManagementSystem.Constants
{
    public class Route
    {
        public const string Account = "Account";
        public const string Update = "Update";
        public const string List = "List";
        public const string Details = "Details";
        public const string Home = "Home";
        public const string Category = "Category";
        public const string User = "User";
        public const string Task = "Task";
        public const string Comment = "Comment";
    }
}
