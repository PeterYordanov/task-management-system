﻿using Microsoft.AspNetCore.Mvc;
using Web.TaskManagementSystem.ViewModels;
using Web.TaskManagementSystem.Services;
using System.Linq;
using System.Web.Mvc;
using Controller = Microsoft.AspNetCore.Mvc.Controller;

namespace Web.TaskManagementSystem.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService<CategoryViewModel> _categoryService;

        public CategoryController(ICategoryService<CategoryViewModel> categoryService)
        {
            _categoryService = categoryService;
        }

        public IActionResult List()
        {
            var categories = _categoryService.List();
            return View(categories);
        }

        public IActionResult Details(int id)
        {
            var category = _categoryService.Get(id);
            return View(category);
        }

        public IActionResult Update(int id)
        {
            var category = _categoryService.Get(id);
            return View(category);
        }

        public IActionResult Edit(CategoryViewModel category)
        {
            _categoryService.Update(category);
            return RedirectToAction(Constants.Route.List, Constants.Route.Category);
        }

        public IActionResult Create()
        {
            if (_categoryService.CanCreate())
            {
                return View(new CategoryViewModel { });
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }

        public IActionResult Insert(CategoryViewModel category)
        {
            _categoryService.Create(category);
            return RedirectToAction(Constants.Route.List, Constants.Route.Category);
        }

        public IActionResult Delete(int id)
        {
            if (_categoryService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Category);
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }
    }
}