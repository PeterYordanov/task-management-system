﻿using Microsoft.AspNetCore.Mvc;
using Web.TaskManagementSystem.ViewModels;
using Web.TaskManagementSystem.Services;
namespace Web.TaskManagementSystem.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService<CommentViewModel> _commentService;
        private readonly ITaskService<TaskViewModel> _taskService;
        private readonly IUserService<UserViewModel> _userService;
        public CommentController(ICommentService<CommentViewModel> commentService, ITaskService<TaskViewModel> taskService, IUserService<UserViewModel> userService)
        {
            _commentService = commentService;
            _taskService = taskService;
            _userService = userService;
        }

        public IActionResult List()
        {
            var comments = _commentService.List();
            return View(comments);
        }

        public IActionResult Details(int id)
        {
            var comment = _commentService.Get(id);
            return View(comment);
        }

        public IActionResult Update(int id)
        {
            var comment = _commentService.Get(id);
            comment.Users = _userService.List();
            comment.Tasks = _taskService.List();
            return View(comment);
        }

        public IActionResult Edit(CommentViewModel comment)
        {
            _commentService.Update(comment);
            return RedirectToAction(Constants.Route.List, Constants.Route.Comment);
        }

        public IActionResult Create()
        {
            if (_commentService.CanCreate())
            {
                var vm = new CommentViewModel
                {
                    Users = _userService.List(),
                    Tasks = _taskService.List()
                };
                return View(vm);
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }

        public IActionResult Insert(CommentViewModel category)
        {
            _commentService.Create(category);
            return RedirectToAction(Constants.Route.List, Constants.Route.Comment);
        }

        public IActionResult Delete(int id)
        {
            if (_commentService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Comment);
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }
    }
}