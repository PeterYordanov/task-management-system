﻿using Microsoft.AspNetCore.Mvc;
using Web.TaskManagementSystem.AuthHandler;
using Web.TaskManagementSystem.HttpHandler;
using Web.TaskManagementSystem.Services;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService<UserViewModel> _userService;

        public UserController(IUserService<UserViewModel> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Account()
        {
            var user = new UserViewModel();
            return View(user);
        }

        [HttpPost]
        public IActionResult SignUp(UserViewModel model)
        {
            if (!string.IsNullOrEmpty(_userService.SignUp(model)))
            {
                return View(model);
            }
            return RedirectToAction("Account", "User");
        }

        [HttpPost]
        public IActionResult SignIn(UserViewModel model)
        {
            return View("Profile", _userService.SignIn(model));
        }

        [HttpGet]
        public IActionResult SignOut()
        {
            _userService.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View(_userService.GetProfile());
        }

        public IActionResult Profile(UserViewModel user)
        {
            if (!user.Password.Equals(user.ConfirmPassword, System.StringComparison.CurrentCulture))
            {
                return View();
            }
            _userService.UpdateProfile(user);
            return RedirectToAction("Account", "User");
        }

        public IActionResult Settings(UserViewModel user)
        {
            _userService.Settings(user);
            return RedirectToAction("Account", "User");
        }


        public IActionResult List()
        {
            var users = _userService.List();
            return View(users);
        }

        public IActionResult Details(int id)
        {
            var user = _userService.Get(id);
            return View(user);
        }

        public IActionResult Update(int id)
        {
            var user = _userService.Get(id);
            return View(user);
        }

        public IActionResult Edit(UserViewModel user)
        {
            _userService.Update(user);
            return RedirectToAction(Constants.Route.List, Constants.Route.User);
        }

        public IActionResult Create()
        {
            if (_userService.CanCreate())
            {
                return View(new UserViewModel { });
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }

        public IActionResult Insert(UserViewModel user)
        {
            _userService.Create(user);
            return RedirectToAction(Constants.Route.List, Constants.Route.User);
        }

        public IActionResult Delete(int id)
        {
            if (_userService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.User);
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }
    }
}