﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Web.TaskManagementSystem.Services;
using Web.TaskManagementSystem.ViewModels;

namespace Web.TaskManagementSystem.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskService<TaskViewModel> _taskService;
        private readonly ICategoryService<CategoryViewModel> _categoryService;
        private readonly IUserService<UserViewModel> _userService;

        public TaskController(ITaskService<TaskViewModel> taskService, ICategoryService<CategoryViewModel> categoryService, IUserService<UserViewModel> userService)
        {
            _taskService = taskService;
            _categoryService = categoryService;
            _userService = userService;
        }

        public IActionResult List(FilterTaskViewModel filter)
        {
            var model = new FilterTaskViewModel();
            var tasks = new List<TaskViewModel>();
            if (filter.Task != null)
            {
                tasks = _taskService.Search(filter);
            }
            else
            {
                tasks = _taskService.List();
            }
            model.Tasks = tasks;
            return View(model);
        }

        public ActionResult Details(int id)
        {
            var task = _taskService.Get(id);
            return View(task);
        }

        public IActionResult Update(int id)
        {
            var task = _taskService.Get(id);
            task.Categories = _categoryService.List();
            task.Users = _userService.List();
            return View(task);
        }

        public IActionResult Edit(TaskViewModel task)
        {
            _taskService.Update(task);
            return RedirectToAction(Constants.Route.List, Constants.Route.Task);
        }

        public IActionResult Create()
        {
            if (_taskService.CanCreate())
            {
                var vm = new TaskViewModel
                {
                    Categories = _categoryService.List(),
                    Users = _userService.List()
                };
                return View(vm);
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }

        public IActionResult Insert(TaskViewModel task)
        {
            _taskService.Create(task);
            return RedirectToAction(Constants.Route.List, Constants.Route.Task);
        }

        public IActionResult Delete(int id)
        {
            if (_taskService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Task);
            }
            return RedirectToAction(Constants.Route.Account, Constants.Route.User);
        }
    }
}