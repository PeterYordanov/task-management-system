# Task Management System

# Task Management System

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/task-management-system/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/task-management-system/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

<p>
<details>
<summary><h2>Screenshots</h2></summary>

![Index](screenshots/Index.png "Index")
![TaskDetails](screenshots/TaskDetails.png "TaskDetails")
![Register](screenshots/Register.jpg "Register")

</details>
</p>


## Tech Stack
- Bootstrap
- ASP.NET Core
- Entity Framework

## System Design
```mermaid
graph TB
  subgraph "Task Management System"
  UI("Task Management UI")
  UI -- invokes --> API("Task Management API")
  API -- invokes --> DAL("Task Management DAL")
  end

  subgraph "MSSQL"
  DB("Database")
  DAL -- queries --> DB
end
```

## Features
- [x] User
	- [x] Profile
	- [x] Sign Up/Sign In
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Task
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Category
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Comment
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List