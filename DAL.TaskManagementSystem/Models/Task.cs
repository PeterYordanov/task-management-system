﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.TaskManagementSystem.Models
{
    public class Task
    {
        [Key]
        public int TaskId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime Deadline { get; set; }

        public DateTime AssignedDate { get; set; }

        public string TaskTitle { get; set; }

        public string Description { get; set; }

        public int AssignedTo { get; set; }

        [ForeignKey("AssignedTo")]
        public virtual User User { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

    }
}
