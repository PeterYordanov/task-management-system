﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.TaskManagementSystem.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Telephone { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        [NotMapped]
        public virtual ICollection<Task> Tasks { get; set; }
    }
}
