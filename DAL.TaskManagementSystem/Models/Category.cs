﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.TaskManagementSystem.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}