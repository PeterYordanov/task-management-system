﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.TaskManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.TaskManagementSystem.Repositories
{
    public class TaskRepository<T> : ITaskRepository<T> where T : class
    {
        protected readonly TaskManagementContext _context;
        protected readonly DbSet<T> _table;

        public TaskRepository(TaskManagementContext context)
        {
            this._context = context;
            this._table = _context.Set<T>();
        }

        public List<Task> GetTasks()
        {
            var query = from task in _context.Task
                        select new Task
                        {
                            TaskId = task.TaskId,
                            TaskTitle = task.TaskTitle,
                            CreatedDate = task.CreatedDate,
                            Description = task.Description,
                            AssignedTo = task.AssignedTo,
                            Category = task.Category,
                            CategoryId = task.CategoryId,
                            AssignedDate = task.AssignedDate,
                            Deadline = task.Deadline,
                            User = task.User
                        };

            var tasks = query.ToList();
            return tasks;
        }

        public Task GetTaskById(int id)
        {
            var query = from task in _context.Task where task.TaskId == id
                        select new Task
                        {
                            TaskId = task.TaskId,
                            TaskTitle = task.TaskTitle,
                            CreatedDate = task.CreatedDate,
                            Description = task.Description,
                            AssignedTo = task.AssignedTo,
                            AssignedDate = task.AssignedDate,
                            Deadline = task.Deadline,
                            User = task.User,
                            Category = task.Category,
                            CategoryId = task.CategoryId,
                            Comments = task.Comments.Select(c => new Comment
                            {
                                CommentId = c.CommentId,
                                Task = c.Task,
                                TaskId = c.TaskId,
                                User = c.User,
                                UserId = c.UserId,
                                CommentText = c.CommentText,
                            })
                            .ToList()
                        };

            var result = query.FirstOrDefault();
            return result;
        }

        public List<Task> SearchTask(string title, string description)
        {
            var query = from task in _context.Task
                        select new Task
                        {
                            TaskId = task.TaskId,
                            TaskTitle = task.TaskTitle,
                            AssignedDate = task.AssignedDate,
                            AssignedTo = task.AssignedTo,
                            CreatedDate = task.CreatedDate,      
                            Deadline = task.Deadline,
                            Description = task.Description,
                            User = task.User,
                            Category = task.Category,
                            CategoryId = task.CategoryId
                        };
            var tasks = query.ToList();

            var filtered = new List<Task>();

            if (title != null)
            {
                filtered = tasks.Where(p => p.TaskTitle.Contains(title)).ToList();
            }
            if (description != null)
            {
                filtered = tasks.Where(p => p.Description == description).ToList();
            }

            return filtered;
        }

        public void Delete(Task task)
        {
            var comments = _context.Comment.Where(p => p.TaskId == task.TaskId).ToList();
            if (comments != null)
            {
                foreach (var comment in comments)
                {
                    _context.Comment.Remove(comment);
                    _context.SaveChanges();
                }
            }
            _context.Task.Remove(task);
            _context.SaveChanges();
        }
    }
}
