﻿using DAL.TaskManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.TaskManagementSystem.Repositories
{
    public interface ITaskRepository<T> where T : class
    {
        List<Task> SearchTask(string title, string description);
        List<Task> GetTasks();
        Task GetTaskById(int it);
        void Delete(Task task);
    }
}
