﻿using DAL.TaskManagementSystem.Models;
using System.Collections.Generic;

namespace DAL.TaskManagementSystem.Repositories
{
    public interface IRepository<T> where T : class
    {
        List<T> GetAll();
        T GetById(int id);
        void Insert(T entity);
        void Edit(T obj);
        void Delete(T obj);
    }
}
