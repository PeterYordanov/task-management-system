﻿using DAL.TaskManagementSystem.Models;

namespace DAL.TaskManagementSystem.Repositories
{
	public interface IUserRepository<T> where T : class
	{
		User Login(string email, string pass);
		User CheckDublicate(string email);
	}
}
