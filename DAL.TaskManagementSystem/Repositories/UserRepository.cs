﻿using DAL.TaskManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL.TaskManagementSystem.Repositories
{
	public class UserRepository<T> : IUserRepository<T> where T : class
	{
		protected readonly TaskManagementContext _context;
		protected readonly DbSet<T> _table;

		public UserRepository(TaskManagementContext context)
		{
			this._context = context;
			this._table = _context.Set<T>();
		}

		public User Login(string email, string pass)
		{
			return _context.User.ToList().Where(p => p.Email == email && p.Password == pass).FirstOrDefault();
		}

		public User CheckDublicate(string email)
		{
			return _context.User.ToList().Where(p => p.Email == email).FirstOrDefault();
		}
	}
}
