﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.TaskManagementSystem.CustomModel
{
    public class FilterTask
    {
        [FromQuery(Name = "TaskTitle")]
        public string TaskTitle { get; set; }
        [FromQuery(Name = "Description")]
        public string Description { get; set; }
    }
}

