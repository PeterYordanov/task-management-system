﻿namespace API.TaskManagementSystem.Extensions
{
    public static class Message
    {
        public const string Conflict = "User with this email already exists.";
        public const string BadRequestUser = "Firstname, lastname, email, password, role are required fields.";
        public const string BadRequestPassword = "Passwords are empty or do not match.";
        public const string Unauthorized = "Wrong email or password.";
        public const string CreatedUser = "Successfully added new user.";
        public const string CreatedCategory = "Successfully added new category.";
        public const string CreatedComment = "Successfully added new comment.";
        public const string CreatedTask = "Successfully added new task.";
    }
}
