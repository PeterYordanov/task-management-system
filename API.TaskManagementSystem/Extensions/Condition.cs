﻿using DAL.TaskManagementSystem.Models;
using System.Collections.Generic;

namespace API.TaskManagementSystem.Extensions
{
    public static class Condition
    {
        public static bool ValidateUser(User user, string firstname, string lastname, string email, string password, string role)
        {
            if (user == null || string.IsNullOrEmpty(firstname) ||
                string.IsNullOrEmpty(lastname) || string.IsNullOrEmpty(email) ||
                string.IsNullOrEmpty(password) || string.IsNullOrEmpty(role))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidatePassword(string password, string confirmPassword)
        {
            if ((string.IsNullOrEmpty(password) && string.IsNullOrEmpty(confirmPassword)) &&
                password != confirmPassword)

            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool ValidateObjects(List<User> users)
        {
            if (users == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Comment> comments)
        {
            if (comments == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Category> categories)
        {
            if (categories == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool ValidateObjects(List<Task> tasks)
        {
            if (tasks == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(User user)
        {
            if (user == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ValidateObject(Comment comment)
        {
            if (comment == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(Category category)
        {
            if (category == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ValidateObject(Task task)
        {
            if (task == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
