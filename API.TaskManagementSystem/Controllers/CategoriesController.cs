﻿using DAL.TaskManagementSystem.Models;
using API.TaskManagementSystem;
using DAL.TaskManagementSystem.Repositories;
using API.TaskManagementSystem.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DAL.TaskManagementSystem;

namespace API.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.AdminBackOffice)]
    public class CategoriesController : ControllerBase
    {
        private readonly IRepository<Category> _repo;

        public CategoriesController(TaskManagementContext context)
        {
            _repo = new Repository<Category>(context);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var categories = _repo.GetAll();
            if (Condition.ValidateObjects(categories))
            {
                return NotFound();
            }
            return Ok(categories);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var category = _repo.GetById(id);
            if (Condition.ValidateObject(category))
            {
                return NotFound();
            }
            return Ok(category);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Category category)
        {
            _repo.Insert(category);
            if (Condition.ValidateObject(category))
            {
                return NotFound();
            }
            return Created(Message.CreatedCategory, category);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Category category)
        {
            _repo.Edit(category);
            return Ok(category);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var category = _repo.GetById(id);
            if (Condition.ValidateObject(category))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(category);
                return Ok();
            }
        }
    }
}
