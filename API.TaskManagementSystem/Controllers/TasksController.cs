﻿using API.TaskManagementSystem.Extensions;
using DAL.TaskManagementSystem.Models;
using DAL.TaskManagementSystem;
using DAL.TaskManagementSystem.Repositories;
using Microsoft.AspNetCore.Mvc;
using DAL.TaskManagementSystem.CustomModel;
using System;

namespace API.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskRepository<Task> _taskRepo;
        private readonly IRepository<Task> _repo;

        public TasksController(TaskManagementContext context)
        {
            _taskRepo = new TaskRepository<Task>(context);
            _repo = new Repository<Task>(context);
        }

        [HttpGet("search")]
        public ObjectResult Search([FromQuery] FilterTask request)
        {
            var tasks = _taskRepo.SearchTask(request.TaskTitle, request.Description);
            return Ok(tasks);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var tasks = _taskRepo.GetTasks();
            if (Condition.ValidateObjects(tasks))
            {
                return NotFound();
            }
            return Ok(tasks);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var task = _taskRepo.GetTaskById(id);
            if (Condition.ValidateObject(task))
            {
                return NotFound();
            }
            return Ok(task);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Task task)
        {
            task.CreatedDate = DateTime.Now;
            _repo.Insert(task);
            if (Condition.ValidateObject(task))
            {
                return NotFound();
            }
            return Created(Message.CreatedTask, task);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Task task)
        {
            _repo.Edit(task);
            return Ok(task);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var task = _repo.GetById(id);
            if (Condition.ValidateObject(task))
            {
                return NotFound();
            }
            else
            {
                _taskRepo.Delete(task);
                return Ok();
            }
        }
    }
}
