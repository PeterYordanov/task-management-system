﻿using DAL.TaskManagementSystem.Models;
using DAL.TaskManagementSystem;
using DAL.TaskManagementSystem.Repositories;
using API.TaskManagementSystem.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    public class CommentsController : ControllerBase
    {
        private readonly IRepository<Comment> _repo;

        public CommentsController(TaskManagementContext context)
        {
            _repo = new Repository<Comment>(context);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var comments = _repo.GetAll();
            if (Condition.ValidateObjects(comments))
            {
                return NotFound();
            }
            return Ok(comments);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var comment = _repo.GetById(id);
            if (Condition.ValidateObject(comment))
            {
                return NotFound();
            }
            return Ok(comment);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Comment comment)
        {
            _repo.Insert(comment);
            if (Condition.ValidateObject(comment))
            {
                return NotFound();
            }
            return Created(Message.CreatedComment, comment);
        }

        [HttpPut]
        [Authorize(Roles = Role.Admin)]
        public IActionResult Put([FromBody] Comment comment)
        {
            _repo.Edit(comment);
            return Ok(comment);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = Role.Admin)]
        public IActionResult Delete(int id)
        {
            var comment = _repo.GetById(id);
            if (Condition.ValidateObject(comment))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(comment);
                return Ok();
            }
        }
    }
}
