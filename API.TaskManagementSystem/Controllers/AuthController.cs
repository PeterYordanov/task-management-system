﻿using DAL.TaskManagementSystem.Models;
using DAL.TaskManagementSystem;
using DAL.TaskManagementSystem.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using API.TaskManagementSystem.Extensions;

namespace API.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUserRepository<User> _userRepo;
        private readonly IRepository<User> _repo;
        private readonly IConfiguration _config;

        public AuthController(TaskManagementContext context, IConfiguration config)
        {
			_userRepo = new UserRepository<User>(context);
            _repo = new Repository<User>(context);
            _config = config;
        }

        [HttpPost("signup")]
        public IActionResult SignUp([FromBody] User user)
        {
            var databaseUser = _userRepo.CheckDublicate(user.Email);

            if (Condition.ValidateUser(user, user.Firstname, user.Lastname, user.Email, user.Password, user.Role))
            {
                return BadRequest(Message.BadRequestUser);
            }
            if (databaseUser != null)
            {
                return Conflict(Message.Conflict);
            }
            else
            {
                user.Firstname = user.Firstname.FirstLetterToUpperCase();
                user.Lastname = user.Lastname.FirstLetterToUpperCase();
                user.Email = user.Email.ToLower();
                user.Role = user.Role.FirstLetterToUpperCase();
                var newUser = new User
                {
                    Firstname = user.Firstname,
                    Lastname = user.Lastname,
                    Email = user.Email,
                    Password = StringExtension.SHA512(user.Password),
                    Role = user.Role,
                    Telephone = user.Telephone
                };
                _repo.Insert(newUser);
                return StatusCode(201, newUser);
            }
        }

        [HttpPost("signin")]
        public IActionResult SignIn([FromBody] User user)
        {
            var hashedPassword = StringExtension.SHA512(user.Password);
            var entity = _userRepo.Login(user.Email.ToLower(), hashedPassword);

            if (entity == null)
            {
                return Unauthorized(Message.Unauthorized);
            }

            // generate token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_config.GetSection("AppSettings:Token").Value);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier,entity.UserId.ToString()),
                    new Claim(ClaimTypes.Email, entity.Email),
                    new Claim(ClaimTypes.Role, entity.Role)
                }),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(
                                     new SymmetricSecurityKey(key),
                                         SecurityAlgorithms.HmacSha512Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            entity.Token = tokenString;
            _repo.Edit(entity);
            return Ok(entity);
        }
    }
}
